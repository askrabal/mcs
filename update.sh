#!/bin/bash

. /home/miner/mcs/server-tools/scripts/util_locking.sh

usage () {
    cat <<EOF
        usage: server_name [[inc|full] n]
EOF
}

if [[ -z "$1" ]]
then
    usage
    exit -1
else
    NAME="$1"; shift
fi

# make sure the folder exists
if [[ ! -d /home/miner/mcs/$NAME ]]
then
    echo -e "folder for server '$NAME' doesn't exist\n" >&2 
    usage
    exit -1
fi

LOCK_DIR="/home/miner/mcs/$NAME/assets/locks"
# MCS_LOCK="$LOCK_DIR/running.lock"
MAP_LOCK="$LOCK_DIR/render.lock"
trap "{ rmdir $MAP_LOCK &> /dev/null; exit -1; }" SIGTERM SIGHUP SIGINT
workers=1

server_running () {
    #if ! check_lock $MCS_LOCK
    if ! tmux has-session -t mcs-$NAME 
    then
        echo "server not running" 1>&2
        exit -1
    fi
}

pushd /home/miner/mcs/$NAME &> /dev/null
# echo "pwd: $(pwd)"

do_render_inc () {
    if ! check_lock $MAP_LOCK
    then
        lock $MAP_LOCK 30s
            echo "incremental render with $1 threads"
            mapcrafter -c render_map.conf -j $1
            mapcrafter_markers -c render_map.conf
            echo "render complete"
        unlock $MAP_LOCK
    else
        exit -1
    fi
}

do_render_full () {
    lock $MAP_LOCK 2m
        echo "full render with $1 threads"
        mapcrafter -c render_map.conf --render-force-all -j $1
        mapcrafter_markers -c render_map.conf
        echo "render complete"
    unlock $MAP_LOCK
}
do_server_save () {
    tmux send-keys -t mcs:0.0 "save-all" 
}

send_server_msg () {
    tmux send-keys -t mcs:0.0 "say $1" 
}

if [[ $# -gt 0 ]]
then
    while [[ $# -gt 0 ]]
    do
	arg="$1"
	case $arg in
	    inc)
        server_running
		do_server_save
		do_render_inc $workers
		send_server_msg "I $(date +'%a %H:%M' ) Maps have been updated visit http://mc.askrabal.org/map/"
		;;
	    full)
        server_running
		do_server_save
		do_render_full $(expr $workers \* 2)
		send_server_msg "F $(date +'%a %H:%M' ) Maps have been updated visit http://mc.askrabal.org/map/"
		;;
            *)
		do_render_inc $workers
		;;
	esac
	shift
    done
else
    do_render_inc $workers
fi

popd &> /dev/null

