#!/bin/bash

. /home/miner/mcs/server-tools/scripts/util_locking.sh

maps="-a "
if [[ $# -eq 0 ]]
then
    echo -e "must specify one or more of the following:\n\tsimple nether end iso" 1>&2
    exit 1
fi

while [[ $# -gt 0 ]]
do
    case $1 in
        simple)
            maps=$maps"map_simple"
            ;;
        nether)
            maps=$maps"map_nether"
            ;;
        end)
            maps=$maps"map_end"
            ;;
        iso)
            maps=$maps"map_iso"
            ;;
    esac
    shift
    if [[ $# -gt 0 ]]
    then
        maps=$maps" -a "
    fi
done

echo "maps==>'$maps'"

NAME="${PWD##*/}"
LOCK_DIR="/home/miner/mcs/$NAME/assets/locks"
MAP_LOCK="$LOCK_DIR/render.lock"

if ! check_lock $MAP_LOCK
then
    echo "quick render starting"
    lock $MAP_LOCK 30s
        mapcrafter -c render_map.conf -j 2 -r ${maps} --batch
        mapcrafter_markers -c render_map.conf
    unlock $MAP_LOCK
else
    echo "map rendering already running"
fi
#maps=${maps}" config.js index.html markers-generated.js markers.js static"
#for m in $maps; 
#do 
#	if [[ ${m:0:1} != "-" ]]
#	then
#		echo "$m"; 
#		rsync -aP --info=progress2 -e ssh --delete-after map_output/${m} web:/home/askrabal/mcs/jungle/
#	fi
#done
echo "quick render complete"
