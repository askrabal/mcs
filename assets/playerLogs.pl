#!/usr/bin/perl

use warnings;
use strict;
use IO::Zlib;


my $logdir = "/home/miner/mcs/jungle/logs";

my $curLog;
open($curLog, "<$logdir/latest.log");


while(<$curLog>) {
    if ( m<\[  (\d\d:\d\d:\d\d)  \] #time
          [^:]*: \s* (.*) #username
          \s*\[ / ( (\d{1,3}\.){3}\d{1,3}:\d*) #user's ip:port
          .*logged>x ) {
        print "$2 @ $1 from $3\n";
    }
    if ( /\[ (\d\d:\d\d:\d\d) \] #time
          [^:]*:\s*(\S*)\s* #username
          .* Disconnected/x) {
        print "$2 stopped @ $1\n";
    }
}
