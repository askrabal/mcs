#!/bin/bash

. ../server-tools/scripts/util_locking.sh
LOCK_DIR="assets/locks/running.lock"
PID_FILE="assets/running.pid"

lock $LOCK_DIR 30s
#java -Xmx192M -jar spigot.jar nogui $* > assets/mcsout < assets/mcsin
java -Xmx512M -jar spigot.jar nogui $* 
unlock $LOCK_DIR
rm -f $PID_FILE &> /dev/null

trap "{ rmdir $LOCK_DIR &> /dev/null;\
        rm -f $PID_FILE &> /dev/null;\
        exit -1; }" EXIT

